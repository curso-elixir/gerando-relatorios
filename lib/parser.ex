defmodule Reports.Parser do
  @moduledoc """
  Documentation for `Reports.Parser`
  """

  def parse_file(filename) do
    "files/#{filename}"
    |> File.stream!()
    |> Stream.map(fn line -> parse_line(line) end)
  end

  defp parse_line(line) do
    line
    |> String.trim()
    |> String.split(",")
    |> convert_numbers_to_integer
  end

  defp convert_numbers_to_integer([head | tail]) do
    tail = Enum.map(tail, fn value -> String.to_integer(value) end)

    [head | tail]
  end
end
